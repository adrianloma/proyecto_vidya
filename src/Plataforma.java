/**
 * Clase Plataforma
 *
 * @author Diego Garza Mendirichaga
 */
import java.awt.Image;
 
public class Plataforma extends Animal{

	/**
	 * Metodo constructor que hereda los atributos de la clase <code>Animal</code>.
	 * @param posX es la <code>posiscion en x</code> del objeto elefante.
	 * @param posY es el <code>posiscion en y</code> del objeto elefante.
	 * @param image es la <code>imagen</code> del objeto elefante.
	 */
	public Plataforma(int posX,int posY,Image image){
		super(posX,posY,image);	
	}
}
